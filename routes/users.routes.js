const {authJwt} = require("../middleware");
const user = require('../controllers/users.controller.js');
const router = require('express').Router();

module.exports = app => {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(`${process.env.API1}/test/all`, user.allAccess);

    app.get(
        `${process.env.API1}/test/user`,
        [authJwt.verifyToken],
        user.userBoard
    );

    app.get(
        `${process.env.API1}/test/mod`,
        [authJwt.verifyToken, authJwt.isModerator],
        user.moderatorBoard
    );

    app.get(
        `${process.env.API1}/test/admin`,
        [authJwt.verifyToken, authJwt.isAdmin],
        user.adminBoard
    );

    router.post('/', user.create);

    router.get('/', user.findAll);

    router.get('/published', user.findAllPublished);

    router.get('/:id', user.findOne);

    router.put('/:id', user.update);

    router.delete('/:id', user.delete);

    router.delete('/', user.deleteAll);

    app.use(`${process.env.API1}/users`, router);
};
