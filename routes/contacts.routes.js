const express = require('express');
const {authJwt} = require("../middleware");
const contact = require('../controllers/contacts.controller.js');
const router = require('express').Router();

module.exports = app => {
    router.post('/', contact.create);
    router.get(
        '/:id',
        // authJwt,
        contact.get,
    );

    router.patch(
        '/:id',
        // authJwt,
        contact.update,
    );

    app.use(`${process.env.API1}/contacts`, router);
};
