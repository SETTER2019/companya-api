const multer = require('multer');
const config = require('../config');
const fileHandler = multer({dest: config.uploads_dir});
const filesParamsValidator = require('../middleware/validators/files.params.validator');
const filesController = require('../controllers/files.controller');
const {authJwt} = require("../middleware");
const company = require('../controllers/companies.controller.js');
const router = require('express').Router();

module.exports = app => {
    router.get('/', company.findAll);

    /**
     * @swagger
     * /api/v1/companies:
     *  post:
     *    description: Информация об объекте.
     *    summary: добавить компанию
     *    requestBody:
     *      content:
     *        application/json:    # Media type
     *          schema:            # Request body contents
     *            $ref: '#/components/schemas/Company'   # Reference to an object
     *          example:           # Child of media type because we use $ref above
     *            # Properties of a referenced object
     *            id: 10
     *            name: Jessica Smith
     *    responses:
     *      '200':
     *        description: A successful response
     *        content:
     *          application/json:
     *            schema:
     *              type: object
     *              properties:
     *                data:
     *                  type: object
     *                  properties:
     *                    id:
     *                      type: integer
     *                      description: ID компании.
     *                      example: 0
     *                    title:
     *                      type: string
     *                      description: Название компании.
     *                      example: Mercedes Benz
     *                    description:
     *                      type: string
     *                      description: Описание компании.
     *                      example: Лучшая компания года.
     *                    published:
     *                      type: boolean
     *                      description: Не понятно.
     *                      example: false
     *                    updatedAt:
     *                      type: string
     *                      description: Время обновления объекта.
     *                      example: 2021-07-08T13:06:28.337Z
     *                    createdAt:
     *                      type: string
     *                      description: Время создания объекта.
     *                      example: 2021-07-08T13:06:28.337Z
     *                    contactId:
     *                      type: integer
     *                      description: Идентификатор контакта.
     *                      example: 1
     *  patch:
     *    description: Обновить информацию об объекте
     *    parameters:
     *      - in: path
     *        name: id
     *        required: true
     *        description: Числовой идентификатор извлекаемой компании.
     *        schema:
     *          type: integer
     */
    router.post('/', company.create);

    /**
     * @swagger
     * /api/v1/companies/{id}:
     *  get:
     *    description: Информация об объекте.
     *    summary: получить одну компанию
     *    parameters:
     *      - in: path
     *        name: id
     *        required: true
     *        description: Идентификатор компании.
     *        schema:
     *          type: integer
     *    responses:
     *      '200':
     *        description: A successful response
     *        content:
     *          application/json:
     *            schema:
     *              type: object
     *              properties:
     *                data:
     *                  type: object
     *                  properties:
     *                    id:
     *                      type: integer
     *                      description: ID компании.
     *                      example: 0
     *                    name:
     *                      type: string
     *                      description: Название компании.
     *                      example: Mercedes Benz
     *  patch:
     *    description: Обновить информацию об объекте
     *    summary: обновить данные компании
     *    parameters:
     *      - in: path
     *        name: id
     *        required: true
     *        description: Числовой идентификатор извлекаемой компании.
     *        schema:
     *          type: integer
     */
    router.get(
        '/:id',
        // authJwt,
        company.findOne,
    );

    router.patch(
        '/:id',
        // authJwt,
        company.update,
    );

    /**
     * @swagger
     * /api/v1/companies:
     *  post:
     *    description: Создать компанию.
     *    summary: создать компанию
     *    parameters:
     *      - in: path
     *        name: id
     *        required: true
     *        description: Идентификатор компании.
     *        schema:
     *          type: integer
     *    responses:
     *      '201':
     *       description: A successful response
     */
    router.post(
        '/:id',
        // authJwt,
        fileHandler.fields([{name: 'file', maxCount: 1}]),
        filesParamsValidator.addCompanyImage,
        filesController.saveImage,
    );

    /**
     * @swagger
     * /api/v1/companies/{id}/image:
     *  post:
     *    description: Добавить картинку компании
     *    responses:
     *      '200':
     *       description: A successful response
     */
    router.post(
        '/:id/image',
        // authJwt,
        fileHandler.fields([{name: 'file', maxCount: 1}]),
        filesParamsValidator.addCompanyImage,
        filesController.saveImage,
    );

    router.delete(
        '/:id/image/:image_name',
        // authJwt,
        filesParamsValidator.removeCompanyImage,
        filesController.removeImage,
    );

    app.use(`${process.env.API1}/companies`, router);
};

