const db = require('../models');
const User = db.user;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {

  if (!req.body.name) {
    res.status(400)
      .send({
        message: 'Content can not be empty!',
      });
    return;
  }

  User.create()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500)
        .send({
          message:
            err.message || 'Some error occurred while creating the User.',
        });
    });
};

exports.findAll = (req, res) => {
  return User.findAll().then((users) => {
    return users;
  });
};

exports.findOne = (req, res) => {

};

exports.update = (req, res) => {

};

exports.delete = (req, res) => {

};

exports.deleteAll = (req, res) => {

};

exports.findAllPublished = (req, res) => {
  User.findAll({ where: { published: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

// Tests
exports.allAccess = (req, res) => {
  res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
  res.status(200).send("User Content.");
};

exports.adminBoard = (req, res) => {
  res.status(200).send("Admin Content.");
};

exports.moderatorBoard = (req, res) => {
  res.status(200).send("Moderator Content.");
};
