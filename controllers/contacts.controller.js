const config = require('../config');

// module.exports = {
//     get,
//     update,
// };

const contact = {
    id: config.contact_id,
    lastname: 'Григорьев',
    firstname: 'Сергей',
    patronymic: 'Петрович',
    phone: '79162165588',
    email: 'grigoriev@funeral.com',
    createdAt: '2020-11-21T08:03:26.589Z',
    updatedAt: '2020-11-23T09:30:00Z',
};
//
// const contact = {
//     "lastname": "Григорьев",
//     "firstname": "Сергей",
//     "patronymic": "Петрович",
//     "phone": "79162165588",
//     "email": "grigoriev@funeral.com",
//     "createdAt": "2020-11-21T08:03:26.589Z",
//     "updatedAt": "2020-11-23T09:30:00Z",
// };

exports.get = (req, res) => {
    return res.status(200).json(contact);
};

exports.create = (contact) => {
    return Contact.create({
        title: contact.title,
        description: contact.description,
    })
        .then((contact) => {
            console.log(">> Created contact: " + JSON.stringify(contact, null, 4));
            return contact;
        })
        .catch((err) => {
            console.log(">> Error while creating contact: ", err);
        });
};
// Получить контакт к данной компании
exports.findContactById = (contactId) => {
    return Contact.findByPk(contactId, {include: ["companys"]})
        .then((contact) => {
            return contact;
        })
        .catch((err) => {
            console.log(">> Error while finding contact: ", err);
        });
};

exports.update = (req, res) => {
    const requestBody = req.body;

    const updatedContact = {...contact};
    Object.keys(requestBody).forEach((key) => {
        updatedContact[key] = requestBody[key];
    });
    updatedContact.updatedAt = new Date();

    return res.status(200).json(updatedContact);
};
