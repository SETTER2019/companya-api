const db = require('../models');
const Company = db.company;
const Op = db.Sequelize.Op;

// const company = {
//   id: config.company_id,
//   contactId: config.contact_id,
//   name: 'ООО Фирма «Перспективные захоронения»',
//   shortName: 'Перспективные захоронения',
//   businessEntity: 'ООО',
//   contract: {
//     no: '12345',
//     issue_date: '2015-03-12T00:00:00Z',
//   },
//   type: ['agent', 'contractor'],
//   status: 'active',
//   createdAt: '2020-11-21T08:03:00Z',
//   updatedAt: '2020-11-23T09:30:00Z',
// };

exports.create = (req, res) => {
    if (!req.body.name) {
        res.status(400)
            .send({
                message: 'Content can not be empty!',
            });
        return;
    }

    const company = {
        name: req.body.name,
        shortName: req.body.shortName,
        // contactId: req.body.contactId,
        businessEntity:req.body.shortName,
        contract:req.body.contract,
        type: req.body.type,
        status: req.body.status,
    };

    Company.create(company)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500)
                .send({
                    message:
                        err.message || 'Произошла ошибка при создании компании.',
                });
        });
};


// ///***
// //**

// module.exports = {
//     get,
//     update,
//     del,
// };

const company = {};
// exports.createCompany = (contactId, company) => {
//     return company.create({
//         name: company.name,
//         text: company.text,
//         contactId: contactId,
//     })
//         .then((company) => {
//             console.log(">> Created company: " + JSON.stringify(company, null, 4));
//             return company;
//         })
//         .catch((err) => {
//             console.log(">> Error while creating company: ", err);
//         });
// };
//
// exports.findCompanyById = (id) => {
//     return Company.findByPk(id, {include: ["contact"]})
//         .then((company) => {
//             return company;
//         })
//         .catch((err) => {
//             console.log(">> Error while finding company: ", err);
//         });
// };
//
// Find a single Company with an id
exports.findOne = (companyId) => {
    return Company.findByPk(companyId).then((company) => {
        return company;
    });
};

exports.findAll = () => {
    return Company.findAll({
        include: ["contacts"],
    }).then((companies) => {
        return companies;
    });
};

exports.get = (req, res) => {
    const URL = _getCurrentURL(req);
    company.photos = [{
        name: '0b8fc462dcabf7610a91.png',
        filepath: `${URL}0b8fc462dcabf7610a91.png`,
        thumbpath: `${URL}0b8fc462dcabf7610a91_160x160.png`,
    }];
    return res.status(200)
        .json(company);
};

exports.update = (req, res) => {
    const requestBody = req.body;

    const URL = _getCurrentURL(req);
    company.photos = [{
        name: '0b8fc462dcabf7610a91.png',
        filepath: `${URL}0b8fc462dcabf7610a91.png`,
        thumbpath: `${URL}0b8fc462dcabf7610a91_160x160.png`,
    }];

    const updatedCompany = {...company};
    Object.keys(requestBody)
        .forEach((key) => {
            updatedCompany[key] = requestBody[key];
        });
    updatedCompany.updatedAt = new Date();

    return res.status(200)
        .json(updatedCompany);
};

exports.del = (req, res) => {
    return res.status(200)
        .end();
};

function _getCurrentURL(req) {
    const {port} = config;
    return `${req.protocol}://${req.hostname}${port === '80' || port === '443' ? '' : `:${port}`}/`;
}
