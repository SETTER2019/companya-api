const app = require('./app');
const cron = require('node-cron');
const fs = require('fs');
const logger = require('./services/logger')(module);
cron.schedule('0 * * * *', () => {
    fs.rm('./public/images/', {
        recursive: true,
        force: true,
    }, (err) => {
        if (err) logger(err);
    });
});
async function start() {
  try {
    app.listen(2114, () => {
      logger.info(`App has been started on http://${process.env.DOMAIN}:${process.env.PORT || 3001}`);
    });
  } catch (error) {
    logger.error(error.message);
    process.exit(1);
  }
}

start();
