module.exports = (sequelize, Sequelize) => {
  const Company = sequelize.define('company', {
    contactId:{
      type: Sequelize.INTEGER,
    } ,
    name: {
      type: Sequelize.STRING,
    },
    shortName: {
      type: Sequelize.STRING,
    },

    businessEntity: {
      type: Sequelize.STRING,
      defaultValue: 'OOO',
    },
    contract: {
      type: Sequelize.JSONB,
    },
    type: {
      type: Sequelize.JSONB,
    },
    status: {
      type: Sequelize.STRING,
      defaultValue: 'active',
    },
  });
  return Company;
};

// const company = {
//   id: config.company_id,
//   contactId: config.contact_id,
//   name: 'ООО Фирма «Перспективные захоронения»',
//   shortName: 'Перспективные захоронения',
//   businessEntity: 'ООО',
//   contract: {
//     no: '12345',
//     issue_date: '2015-03-12T00:00:00Z',
//   },
//   type: ['agent', 'contractor'],
//   status: 'active',
//   createdAt: '2020-11-21T08:03:00Z',
//   updatedAt: '2020-11-23T09:30:00Z',
// };
