module.exports = (sequelize, Sequelize) => {
  const Contact = sequelize.define('contacts', {
    title: {
      type: Sequelize.STRING,
    },
    description: {
      type: Sequelize.STRING,
    },
    published: {
      type: Sequelize.BOOLEAN,
    },
  });

  return Contact;
};
