require('dotenv').config();

const express = require('express');
const httpContext = require('express-http-context');
const marked = require('marked');
const bodyParser = require("body-parser");
const cors = require("cors");
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');



const swaggerOptions = {
    swaggerDefinition: {
        openapi: '3.0.0',
        info: {
            title: 'Company API',
            version: '1.0.0',
            description: 'Company API Information',
            contact: {
                name: 'SETTER2000 Developer',
                url: 'https://github.com/SETTER2000/api-company',
            },
            servers: [
                {
                    url: `http://localhost:${process.env.PORT || 3001}`,
                    description: 'Development server',
                },
            ],
        },
    },
    apis: ['app.js', './routes/*.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
const app = express();
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use(httpContext.middleware);
app.use((req, res, next) => {
    httpContext.ns.bindEmitter(req);
    httpContext.ns.bindEmitter(res);
    httpContext.set('method', req.method);
    httpContext.set('url', req.url);
    next();
});

app.use(cors());
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(`${__dirname}/public`));
const db = require("./models");


require('./routes/auth.routes')(app);
require('./routes/users.routes')(app);
require('./routes/companies.routes')(app);
require('./routes/contacts.routes')(app);

app.get('/', (req, res) => {
    const path = `${__dirname}/README.md`;
    const file = fs.readFileSync(path, 'utf8');
    const pageContent = marked(file.toString());
    res.send(
        `
    <html>
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="github-markdown.css">
      </head>
      <body>
        <article class="markdown-body">${pageContent}</article>
      </body>
    </html>
    `,
    );
});

module.exports = app;
