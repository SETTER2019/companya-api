const request = require('supertest');
const app = require('../app');
api = process.env.API1;
pull = {};
authorize = async () => {
   let response =  await request(app).post(`${api}/auth/signin`).send({
        "name": "admin",
        "password": "admin"
    });


    pull.authUser = response.body.accessToken;
    return  response;
};
describe('Авторизация:', () => {
    it(`авторизация админа успешна`, async () => {
        let response = await authorize();
        expect(response.statusCode).toBe(200);

    });
    it(`тип данных json`, async () => {
        const response = await request(app).post(`${api}/auth/signin`).send({
            "name": "admin",
            "password": "admin"
        });
        expect(response.headers['content-type']).toEqual(expect.stringContaining('json'));
    });
});
beforeEach(async function beforeEach() {
    // this.timeout(16000);
    try {
     await authorize();

    } catch (e) {

        throw e;
    }
});
module.exports = pull;
