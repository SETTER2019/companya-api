create TABLE company(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    shortName VARCHAR(255),
    businessEntity VARCHAR(255),
    contract VARCHAR(255),
    type VARCHAR(255),
    status VARCHAR(255),
    createAt VARCHAR(255),
    updateAt VARCHAR(255),
    contactId INTEGER,
    FOREIGN KEY (contactId) REFERENCES contact (id)
);

create TABLE contact(
    id SERIAL PRIMARY KEY,
    lastname VARCHAR(255),
    firstname VARCHAR(255),
    patronymic VARCHAR(255),
    phone VARCHAR(255),
    email VARCHAR(255),
    createAt VARCHAR(255),
    updateAt VARCHAR(255)
);
